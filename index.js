//get random int number from 1 to max
function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

//form array of random int numbers and add them to DOM
function formRandArr (e) {
    if (e.target.classList.contains("button_disabled")) { //prevent handling when button disabled
        return;
    }

    let numbersList = document.getElementsByClassName("numbers-list")[0],
        sortBtn = document.getElementsByClassName("button-container")[0].children[1],
        numbers = [];

    while (numbersList.firstChild) { //removing previous number set from DOM
        numbersList.removeChild(numbersList.firstChild);
    }

    for (let step = 1; step < 10; step++) { //creating span element with random number and adding it to the DOM
        let span = document.createElement('SPAN'),
            number = getRandomInt(99);

        numbers.push(number);
        span.classList.add('list__item');
        span.innerText = number;
        numbersList.appendChild(span);
    }

    //removing old event by cloning button (events doesnt clone)
    //this is needed not to add multiple events for one button
    let newSortBtn = sortBtn.cloneNode(true);
    sortBtn.parentNode.insertBefore(newSortBtn, sortBtn);
    sortBtn.parentNode.removeChild(sortBtn);

    newSortBtn.addEventListener('click', function () {
        sortArr(numbers, numbers.length - 1);
    }, {once: true}); //creating singleton event listener for sort button
}

//sort given array of numbers
//maxEl prevents iterations on already sorted part of array
async function sortArr(arr, maxEl) {
    function sleep (ms) { //custom sleep function, gives animations time to work out. ms - time to sleep in milliseconds
        return new Promise(function(resolve) {
            setTimeout(resolve, ms);
        })
    }

    let buttonContainer = document.getElementsByClassName("button-container")[0],
        setBtn = buttonContainer.children[0],
        sortBtn = buttonContainer.children[1],
        numbersList = document.getElementsByClassName("numbers-list")[0],
        newMaxEl = maxEl,
        count = 0;

    if (!buttonContainer.getElementsByClassName("button_disabled")[0]) { //setting disabled cls while sorting
        setBtn.classList.add("button_disabled");
        sortBtn.classList.add("button_disabled");
    }

    for (let i in arr) {
        let num = arr[i],
            next_i = parseInt(i)+1,
            nextNum = arr[next_i];

        if (i < maxEl) {
            addRemoveAnim(numbersList.children[i], numbersList.children[next_i], "list__item_step", 1000); //showing compared elements
            await sleep(1000);
            if (num > nextNum) {
                addRemoveAnim(numbersList.children[i], numbersList.children[next_i], "list__item_unmatch", 1000); //showing that sort order is wrong
                await sleep(1000);
                addRemoveAnim(numbersList.children[i], numbersList.children[next_i], "fade_in", 750);
                await sleep(750);
                arr[i] = nextNum;
                numbersList.children[i].innerText = nextNum;
                arr[next_i] = num;
                numbersList.children[next_i].innerText = num;
                addRemoveAnim(numbersList.children[i], numbersList.children[next_i], "fade_out", 750);
                await sleep(750);
                newMaxEl = parseInt(i); //setting new max el
                count += 1; //count allows to exit recursion
            }
        } else {
            break;
        }
        await sleep(100);
    }
    if (count) { //continue recursion
        sortArr(arr, newMaxEl);
    } else { //showing that sort is complete
        for (let i in arr) {
            numbersList.children[i].classList.add("list__item_sorted");
        }
        //enable buttons
        setBtn.classList.remove("button_disabled");
        sortBtn.classList.remove("button_disabled");
    }
}

//adds css animation class to comparing elements and removes it on interval
function addRemoveAnim(firstEl, secondEl, className, interval) {
    firstEl.classList.add(className);
    secondEl.classList.add(className);
    setTimeout(function () {
        firstEl.classList.remove(className);
        secondEl.classList.remove(className);
    }, interval)
}